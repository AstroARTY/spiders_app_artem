import math
import itertools

from constants import OPERATORS


class Pagination:
    def __init__(self, page, per_page, collection, link, filters):
        self.page = page or 1
        self.per_page = per_page or 10
        self.collection = collection
        self.link = link
        self.filters = filters or {}
        self.total = self.total()

    def items(self):
        return list(self.collection.find(self.filters).skip((self.page-1)*self.per_page).limit(self.per_page))

    def total(self):
        return self.collection.count()

    def links(self):
        if self.page == 1:
            html_links = '<li class="disabled"><a href="#">&laquo;</a></li>'
        else:
            html_links = '<li><a href="{link}?page={page}">&laquo;</a></li>'.format(link=self.link, page=self.page-1)

        total_pages = int(math.ceil(self.total / float(self.per_page)))
        for i in range(1, total_pages+1):
            if i != self.page:
                html_links += '<li><a href="{link}?page={page}">{page}</a></li>'.format(page=i, link=self.link)
            else:
                html_links += '<li class="active"><a href="{link}?page={page}">{page}</a></li>'.format(
                    page=i, link=self.link)

        if self.page == total_pages:
            html_links += '<li class="disabled"><a href="#">&raquo;</a></li>'
        else:
            html_links += '<li><a href="{link}?page={page}">&raquo;</a></li>'.format(link=self.link, page=self.page+1)

        return html_links


def make_filters(data):
    splited_data = data.split("&")
    splited_filters = split_seq(splited_data)
    filters = dict()
    for f in splited_filters:
        if len(f[-1]) > 7:
            if f[1].split("=")[-1] == "Equal":
                filters[f[0].split("=")[-1]] = f[2].split("=")[-1].replace("+", " ")
            else:
                filters[f[0].split("=")[-1]] = {
                    OPERATORS[f[1].split("=")[-1].replace("+", " ")]: f[2].split("=")[-1].replace("+", " ")
                }
    return filters


def split_seq(iterable, size=3):
    it = iter(iterable)
    item = list(itertools.islice(it, size))
    while item:
        yield item
        item = list(itertools.islice(it, size))